<footer role="contentinfo">
	<div class="footer-logo"></div>
	<div class="container">
		<?php if (have_rows('office_locations','option')): ?>
		<div class="address">
			<?php while (have_rows('office_locations','option')) : the_row(); ?>
			<?php if(get_sub_field('phone_number')):
				$phone = get_sub_field('phone_number');
				$phoneTrimmed = preg_replace("/[^0-9]/", "", $phone);
				endif;
			?>
			<div class="hcard">
				<span class="p-name">Taggart Architects</span><br/>
				<span class="adr"><span class="p-street-address"><?php the_sub_field('street_address');?></span><br/>
				<span class="p-locality"><?php the_sub_field('city');?></span>, <span class="p-region">AR</span> <span class="p-postal-code"><?php the_sub_field('zip_code');?></span></span><br/>
				<?php if(get_sub_field('phone_number')) :?>
				<a href="tel:+1<?php echo $phoneTrimmed; ?>" class="p-tel"><?php echo $phone;?></a>
				<?php endif; ?>
			</div>
			<?php endwhile; ?>
		</div>
		<?php endif;?>
		<nav id="footer-nav" class="footer-nav">
			<ol>
				<li class="nav-profile"><a href="/profile/">Profile</a></li>
				<li class="nav-projects"><a href="/projects/">Projects</a></li>
				<li class="nav-news"><a href="/news/">News</a></li>
				<li class="nav-contact"><a href="/contact/">Contact</a></li>
			</ol>
		</nav>
		<div class="social-links">
			<ol>
				<li class="facebook"><a href="https://www.facebook.com/pages/TAGGART-Architects/171155099600266?viewer_id=20613717">Facebook</a></li>
				<li class="twitter"><a href="https://twitter.com/TaggArch">Twitter</a></li>
			</ol>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>