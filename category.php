<?php
	$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri_segments = explode('/', $uri_path);
	$page_url = $uri_segments[2];
?>

<?php get_header(); ?>
<body class="category-project-listing category-<?php echo $page_url; ?>">
	<?php include '_includes/banner.php'; ?>
	<div id="content" role="main">
		<ol class="mobile-subnav">
			<li class="nav-all"><a href="/projects">All Projects</a></li>
			<li class="nav-healthcare"><a href="/category/healthcare">Healthcare</a></li>
			<li class="nav-education"><a href="/category/education">Education</a></li>
			<li class="nav-government-and-civic"><a href="/category/government-and-civic">Government &amp; Civic</a></li>
			<li class="nav-sports-and-recreation"><a href="/category/sports-and-recreation">Sports &amp; Recreation</a></li>
			<li class="nav-commercial"><a href="/category/commercial">Commercial</a></li>
			<li class="nav-sustainable-design"><a href="/category/sustainable-design">Sustainable Design</a></li>
			<li class="nav-on-the-boards"><a href="/category/on-the-boards">On The Boards</a></li>
		</ol>
	<?php if ( have_posts() ) : ?>
		<div class="project-listing">
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="project">
				<?php $image = get_field('project_featured_image');
					if (!is_array($image)) {
						$image = acf_get_attachment($image);
					}
					$url = $image['url'];
				?>
				<div class="project-image">
					<a href="<?php the_permalink();?>">
						<img src="<?php echo $url;?>" alt="" />
						<span class="view-bio">View Project</span>
					</a>
				</div>
				<div class="project-title">
					<h4><?php the_title();?></h4>
				</div>
			</div>
		<?php endwhile;?> 
		</div>
	<?php endif;?>
	</div>
	<?php get_footer(); ?>
</body>
</html>