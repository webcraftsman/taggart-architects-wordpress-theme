<?php get_header(); ?>
<body id="page-<?php echo $post->post_name; ?>" class="page-<?php echo $post->post_name; ?> page-project-listing">
	<?php include '_includes/banner.php'; ?>
	<div id="content" role="main">
		
		<ol class="mobile-subnav">
			<li class="nav-all"><a href="/projects">All Projects</a></li>
			<li class="nav-healthcare"><a href="/category/healthcare">Healthcare</a></li>
			<li class="nav-education"><a href="/category/education">Education</a></li>
			<li class="nav-government-and-civic"><a href="/category/government-and-civic">Government &amp; Civic</a></li>
			<li class="nav-sports-and-recreation"><a href="/category/sports-and-recreation">Sports &amp; Recreation</a></li>
			<li class="nav-commercial"><a href="/category/commercial">Commercial</a></li>
			<li class="nav-sustainable-design"><a href="/category/sustainable-design">Sustainable Design</a></li>
			<li class="nav-on-the-boards"><a href="/category/on-the-boards">On The Boards</a></li>
		</ol>
		
	<?php
		$args= array(
			'post_type' => 'project',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'posts_per_page' => -1
		);
		$projects_query = new WP_Query($args);
	?>
		<?php if( $projects_query->have_posts() ): ?>
		<div class="project-listing">
		<?php while( $projects_query->have_posts() ) : $projects_query->the_post();?>
			<div class="project <?php foreach((get_the_category()) as $category) { echo $category->slug . ' '; } ?>">
				<?php $image = get_field('project_featured_image');
					$url = $image['url'];
					$w1200 = $image['sizes']['listing-1200'];
					$w1026 = $image['sizes']['listing-1026'];
					$w833 = $image['sizes']['listing-833'];
					$w605 = $image['sizes']['listing-605'];
					$w300 = $image['sizes']['listing-300'];
				?>
				<a href="<?php the_permalink();?>">
					<div class="project-image">
						<img src="<?php echo $w1200; ?>" 
							sizes="(min-width: 960px) 300px,
							(min-width: 768px) calc(32vw - 34px),
							(min-width: 480px) calc(48vw - 34px),
							calc(100vw - 34px)"
							srcset="<?php echo $w300; ?> 300w,
									<?php echo $w605;?> 605w,
									<?php echo $w833;?> 833w,
									<?php echo $w1026;?> 1026w,
									<?php echo $w1200;?> 1200w"			
							alt="" />
						<span class="view-bio">View Project</span>
					</div>
					<div class="project-title">
						<h4><?php the_title();?></h4>
					</div>
				</a>
			</div>
		<?php endwhile; endif;?>
		</div>
	</div>
	<?php get_footer(); ?>
</body>
</html>