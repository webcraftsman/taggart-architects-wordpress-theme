<?php $map = get_field('google_map_api_key','option'); ?>
<?php get_header(); ?>
<body id="page-<?php echo $post->post_name; ?>" class="page-<?php echo $post->post_name; ?> secondary-page single-project-page">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php if( !empty($map) ): ?>
	<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $map;?>"></script>
	<script type="text/javascript">
		jQuery(function ($) {
		    $( document ).ready( function() {
				//Google Maps JS
				//Set Map
				function initialize() {
					var $lat = $('.marker').data('lat');
					var $lng = $('.marker').data('lng');
					var myLatlng = new google.maps.LatLng($lat,$lng);
					var mapOptions = {
						zoom: 15,
						center: myLatlng,
						scrollwheel: false
					}
			
					var map = new google.maps.Map(document.getElementById('acf-map'), mapOptions);
					//Callout Content
					var contentString = 'Some address here..';
					//Set window width + content
					var infowindow = new google.maps.InfoWindow({
						content: contentString,
						maxWidth: 500
					});
			
					//Add Marker
					var marker = new google.maps.Marker({
						position: myLatlng,
						map: map,
						title: '<?php the_title();?>'
					});
			
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map,marker);
					});
			
					//Resize Function
					google.maps.event.addDomListener(window, "resize", function() {
						var center = map.getCenter();
						google.maps.event.trigger(map, "resize");
						map.setCenter(center);
					});
				}
			
				google.maps.event.addDomListener(window, 'load', initialize);
			});
		})
	</script>
	<?php endif; ?>
	<?php include '_includes/banner.php'; ?>
	<div id="content">
		<ol class="mobile-subnav">
			<li class="nav-all"><a href="/projects">All Projects</a></li>
			<li class="nav-healthcare"><a href="/category/healthcare">Healthcare</a></li>
			<li class="nav-education"><a href="/category/education">Education</a></li>
			<li class="nav-government-and-civic"><a href="/category/government-and-civic">Government &amp; Civic</a></li>
			<li class="nav-sports-and-recreation"><a href="/category/sports-and-recreation">Sports &amp; Recreation</a></li>
			<li class="nav-commercial"><a href="/category/commercial">Commercial</a></li>
			<li class="nav-sustainable-design"><a href="/category/sustainable-design">Sustainable Design</a></li>
			<li class="nav-on-the-boards"><a href="/category/on-the-boards">On The Boards</a></li>
		</ol>
		<?php if( have_rows('project_carousel_images')):
			$count = 0;
			while( have_rows('project_carousel_images')) : the_row();
			$count++;
			endwhile;
		endif;?>		
		<?php if( have_rows('project_carousel_images') && $count > 1 ): ?>
		<div id="hero-carousel" class="hero-carousel">
			<ol>
				<?php $featureImage = get_field('project_featured_image');
					  $url = $featureImage['url'];
					  $imageBase = substr($url, 0, -4);
					  $w1518 = $featureImage['sizes']['project-1518'];
					  $w1222 = $featureImage['sizes']['project-1222'];
					  $w844 = $featureImage['sizes']['project-844'];
					  $w300 = $featureImage['sizes']['project-300'];
				?>
				<li class="hero-slide">
					<a href="<?php echo $w1518;?>" data-modal="<?php echo $imageBase;?>">
					<img src="<?php echo $w1222; ?>" 
						sizes="(min-width: 768px) 596px,
						(min-width: 660px) 500px,
						100vw"
						srcset="<?php echo $w300; ?> 300w,
								<?php echo $w844;?> 844w,
								<?php echo $w1222;?> 1222w,
								<?php echo $w1518;?> 1518w"			
						alt="" />
					</a>
				</li>
			<?php if( have_rows('project_carousel_images')):
				while( have_rows('project_carousel_images')) : the_row();?>
				<?php $carouselImage = get_sub_field('add_carousel_image');
					  $url2 = $carouselImage['url'];
					  $imageBase2 = substr($url2, 0, -4);
					  $c1518 = $carouselImage['sizes']['project-1518'];
					  $c1222 = $carouselImage['sizes']['project-1222'];
					  $c844 = $carouselImage['sizes']['project-844'];
					  $c300 = $carouselImage['sizes']['project-300'];
				?>
				<li class="hero-slide">
					<a href="<?php echo $c1222;?>" data-modal="<?php echo $imageBase2;?>">
					<img src="<?php echo $c1222; ?>" 
						sizes="(min-width: 768px) 596px,
						(min-width: 660px) 500px,
						100vw"
						srcset="<?php echo $c300; ?> 300w,
								<?php echo $c844;?> 844w,
								<?php echo $c1222;?> 1222w,
								<?php echo $c1518;?> 1518w"			
						alt="" />
					</a>
				</li>
				<?php endwhile;
			endif;?>
			</ol>
		</div>
		<?php else: ?>
		<div class="hero-image">
			<?php $featureImage = get_field('project_featured_image');
				  $url = $featureImage['url'];
				  $imageBase = substr($url, 0, -4);
				  $w1518 = $featureImage['sizes']['project-1518'];
				  $w1222 = $featureImage['sizes']['project-1222'];
				  $w844 = $featureImage['sizes']['project-844'];
				  $w300 = $featureImage['sizes']['project-300'];
			?>
			<div class="hero-project">
				<a href="<?php echo $w1518;?>" data-modal="<?php echo $imageBase;?>">
				<img src="<?php echo $w1222; ?>" 
					sizes="(min-width: 768px) 596px,
					(min-width: 660px) 500px,
					100vw"
					srcset="<?php echo $w300; ?> 300w,
							<?php echo $w844;?> 844w,
							<?php echo $w1222;?> 1222w,
							<?php echo $w1518;?> 1518w"			
					alt="" />
				</a>
			</div>
		</div>
		<?php endif;?>
		<div class="wrapper two-column">
			<div class="content-main" role="main">
				<h2 class="page-title"><?php the_title(); ?></h2>
				<div class="body-content">
					<?php the_field('project_writeup'); ?>
				</div>
			</div>
			<aside class="content-secondary" role="complementary">
				<h3 class="aside-heading">Project Details</h3>
				<?php if( get_field('project_client') ): ?>
				<div class="client"><strong>Client:</strong> <?php the_field('project_client'); ?></div>
				<?php endif; ?>
				<?php if( get_field('project_date_of_completion') ): ?>
				<div class="completion-date"><strong>Date of Completion:</strong> <?php the_field('project_date_of_completion'); ?></div>
				<?php endif; ?>
				<?php if( get_field('project_construction_cost') ): ?>
				<div class="construction-cost"><strong>Construction Cost:</strong> <?php the_field('project_construction_cost'); ?></div>
				<?php endif; ?>
				<?php if( get_field('project_area') ): ?>
				<div class="area"><strong>Area:</strong> <?php the_field('project_area'); ?> sq ft</div>
				<?php endif; ?>
				<?php if( get_field('project_engineer') ): ?>
				<div class="engineer"><strong>Engineer:</strong> <?php the_field('project_engineer'); ?></div>
				<?php endif; ?>
				<?php if( get_field('project_certifications') ): ?>
				<div class="certification"><strong>Certifications:</strong> <?php the_field('project_certifications'); ?></div>
				<?php endif; ?>
				<?php if( get_field('project_awards') ): ?>
				<div class="awards"><strong>Awards:</strong> <?php the_field('project_awards'); ?></div>
				<?php endif; ?>
			</aside>
		</div>
		<?php if( get_field('project_secondary_images') ): ?>
		<div class="project-development">
			<h3 class="section-heading">Project Development</h3>
			<ol id="projects-carousel" class="projects-carousel">
			<?php if( have_rows('project_secondary_images')):
				while( have_rows('project_secondary_images')) : the_row();?>
				<?php $projectImage = get_sub_field('add_secondary_image');
					  $fullUrl = $projectImage['url'];
					  $imageBase3 = substr($fullUrl, 0, -4);
					  $mobileModal = $projectImage['sizes']['project-1518'];
					  $sc1200 = $projectImage['sizes']['carousel-1200'];
					  $sc1027 = $projectImage['sizes']['carousel-1027'];
					  $sc833 = $projectImage['sizes']['carousel-833'];
					  $sc606 = $projectImage['sizes']['carousel-606'];
					  $sc300 = $projectImage['sizes']['carousel-300'];
					  
				?>
				<li class="project-dev">
					<a href="<?php echo $mobileModal;?>" data-modal="<?php echo $imageBase3;?>" data-caption="<?php the_sub_field('secondary_caption');?>">
					<img src="<?php echo $sc606; ?>" 
						sizes="(min-width: 768px) 220px,
						(min-width: 600px) calc(48vw - 34px),
						100vw"
						srcset="<?php echo $sc300; ?> 300w,
								<?php echo $sc606;?> 606w,
								<?php echo $sc833;?> 833w,
								<?php echo $sc1027;?> 1027w,
								<?php echo $sc1200;?> 1200w"			
						alt="" />
					</a>
				</li>
				<?php endwhile;
			endif;?>
			</ol>
		</div>
		<?php endif; ?>
		<?php if( !empty($map) ): ?>
		<?php 
			$location = get_field('project_location');
		
			if( !empty($location) ):
		?>
		<div class="project-location">
			<h3 class="section-heading">Location</h3>
			<div id="map">
				<div id="acf-map">
					<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php endif; ?>
	</div>
	<?php endwhile; endif;?>
	<?php get_footer(); ?>
</body>
</html>