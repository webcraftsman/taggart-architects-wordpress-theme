<?php 
	function taggart_styles() {
		wp_enqueue_style('screen', get_template_directory_uri().'/_css/main.min.css','','1.0');
	}
	add_action('wp_enqueue_scripts','taggart_styles');
	
	function taggart_scripts() {
		$base = get_template_directory_uri();
		wp_enqueue_script('jquery',true);
		if	(is_single()) {
		wp_enqueue_script('slick', $base.'/_js/slick.min.js', array('jquery'), '1.0.0', true);
		}
		wp_enqueue_script('main', $base . '/_js/main.min.js',array('jquery'), '1.0.0', true);
	}
	add_action('wp_enqueue_scripts','taggart_scripts');
	
	function my_acf_init() {
		acf_update_setting('google_api_key', 'AIzaSyDnjGpr2FwQMeaB_IL_Ahb8LdDyeM-5Dh4');
	}

	add_action('acf/init', 'my_acf_init');
	
	// Do not load Contact Form 7 js and css on every page
	add_filter( 'wpcf7_load_js', '__return_false' );
	add_filter( 'wpcf7_load_css', '__return_false' );
	
	
	function add_custom_types_to_tax( $query ) {
		if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
		
		// Get all your post types
		$post_types = array( 'post', 'project' );
		
		$query->set( 'post_type', $post_types );
		return $query;
		}
	}
	add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );
	
	// Remove max_srcset_image_width
	function remove_max_srcset_image_width( $max_width ) {
     	return false;
 	}
 	add_filter( 'max_srcset_image_width', 'remove_max_srcset_image_width' );
 	
 	
 	/*
 * Remove Original Uploaded images
 * retain large size image
 * https://phpbits.net/wordpress-replace-user-uploaded-large-images-automatically/
 */
 
	add_filter('wp_generate_attachment_metadata','phpbits_replace_uploaded_image');
	function phpbits_replace_uploaded_image($image_data) {
	// if there is no large image : return
	if (!isset($image_data['sizes']['thumbnail'])) return $image_data;
	// paths to the uploaded image and the large image
	$upload_dir = wp_upload_dir();
	$uploaded_image_location = $upload_dir['basedir'] . '/' .$image_data['file'];
	$large_image_location = $upload_dir['path'] . '/'.$image_data['sizes']['thumbnail']['file']; // ** This only works for new image uploads
	// delete the uploaded image
	unlink($uploaded_image_location);
	// rename the large image
	rename($large_image_location,$uploaded_image_location);
	// update image metadata and return them
	$image_data['width'] = $image_data['sizes']['thumbnail']['width'];
	$image_data['height'] = $image_data['sizes']['thumbnail']['height'];
	unset($image_data['sizes']['thumbnail']);
	return $image_data;
	}
 	
 	
 	if ( function_exists( 'add_theme_support' ) ) {
 		add_theme_support( 'post-thumbnails' );
// 		add_image_size('full-3000', 3000, 9999);
//		add_image_size('full-2785', 2875, 9999);
//		add_image_size('full-2560', 2560, 9999);
//		add_image_size('full-2322', 2322, 9999);
//		add_image_size('full-2074', 2074, 9999);
//		add_image_size('full-1817', 1817, 9999);
//		add_image_size('full-1518', 1518, 9999);
//		add_image_size('full-1222', 1222, 9999);
//		add_image_size('full-844', 844, 9999);
//		add_image_size('full-300', 300, 9999);
 		
//		add_image_size('project-3000', 3000, 2013, array('center', 'center'));
		add_image_size('project-2785', 2875, 1869, array('center', 'center'));
		add_image_size('project-2560', 2560, 1718, array('center', 'center'));
		add_image_size('project-2322', 2322, 1558, array('center', 'center'));
		add_image_size('project-2074', 2074, 1392, array('center', 'center'));
		add_image_size('project-1817', 1817, 1219, array('center', 'center'));
		add_image_size('project-1518', 1518, 1019, array('center', 'center'));
		add_image_size('project-1222', 1222, 820, array('center', 'center'));
		add_image_size('project-844', 844, 566, array('center', 'center'));
		add_image_size('project-300', 300, 201, array('center', 'center'));
		
		add_image_size('listing-1200', 1200, 880, array('center', 'center'));
		add_image_size('listing-1026', 1026, 752, array('center', 'center'));
		add_image_size('listing-833', 833, 611, array('center', 'center'));
		add_image_size('listing-605', 605, 444, array('center', 'center'));
		add_image_size('listing-300', 300, 220, array('center', 'center'));
		
		add_image_size('carousel-1200', 1200, 873, array('center', 'center'));
		add_image_size('carousel-1027', 1027, 747, array('center', 'center'));
		add_image_size('carousel-833', 833, 606, array('center', 'center'));
		add_image_size('carousel-606', 606, 441, array('center', 'center'));
		add_image_size('carousel-300', 300, 218, array('center', 'center'));
		
		add_image_size('weekend-1200', 1200, 9999);
		add_image_size('weekend-1026', 1026, 9999);
		add_image_size('weekend-833', 833, 9999);
		add_image_size('weekend-605', 605, 9999);
		add_image_size('weekend-300', 300, 9999);
		
		add_image_size('bio-1880', 1800, 1379);
		add_image_size('bio-1536', 1536, 1127);
		add_image_size('bio-940', 940, 690);
		add_image_size('bio-768', 768, 563);
		add_image_size('bio-300', 300, 220);
		
		add_image_size('news-1536',1536, 9999);
		add_image_size('news-960',960, 9999);
		add_image_size('news-768',768, 9999);
		add_image_size('news-480',480, 9999);
		
		
	}
 	
 	function remove_menus(){
		remove_menu_page( 'edit.php' );
		remove_menu_page( 'edit-comments.php' );
	}
	add_action( 'admin_menu', 'remove_menus' );
	
	if( function_exists('acf_add_options_page') ) {
	
		acf_add_options_page();
		
	}
	
	
	function my_custom_login_logo() {
		$url = get_field('login_screen_background', 'option');
	    echo '<style type="text/css">
	        .login h1 a { background-image:url('.get_bloginfo('template_directory').'/_img/sprite-2x.png) !important; background-position: 0 -35px !important; background-size:250px 500px !important; width:146px !important; height:27px !important;}
	        #loginform {background:rgba(255,255,255,.4) !important;}
	        #loginform label {color:#fff !important;}
	        body { background:url('.$url.') no-repeat center top !important; background-size:cover !important;}
	        #nav a,#backtoblog a {color:#fff !important;}
        #backtoblog {padding-left:7px !important;)
	    </style>';
	}
	add_action('login_head', 'my_custom_login_logo');

?>