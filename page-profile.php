<?php get_header(); ?>
<body id="page-<?php echo $post->post_name; ?>" class="page-<?php echo $post->post_name; ?> secondary-page">
	<?php include '_includes/banner.php'; ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="content">
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="hero-image"><?php the_post_thumbnail(); ?></div>
		<?php endif; ?>
		<div class="wrapper two-column">
			<div class="content-main" role="main">
				<h2 class="page-title"><?php the_field('profile_headline'); ?></h2>
				<div class="body-content"><?php the_field('profile_body_content');?></div>
			</div>
			<aside class="content-secondary" role="complementary">
				<h3 class="aside-heading">History</h3>
				<div class-"body-content"><?php the_field('profile_history'); ?></div>
			</aside>
		</div>
		<?php wp_reset_query(); ?>
		<div class="our-people">
			<h3 class="section-heading">Our People</h3>
			<?php
				$args= array(
					'post_type' => 'people',
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'posts_per_page' => -1
				);
				$people_query = new WP_Query($args);
			?>
			<?php if( $people_query->have_posts() ): ?>
			<div class="wrapper">
				<?php while( $people_query->have_posts() ) : $people_query->the_post();?>
				<div class="person">
					<?php $image = get_field('bio_image');
						  $url = $image['url'];
						  $w1200 = $image['sizes']['listing-1200'];
						  $w1026 = $image['sizes']['listing-1026'];
						  $w833 = $image['sizes']['listing-833'];
						  $w605 = $image['sizes']['listing-605'];
						  $w300 = $image['sizes']['listing-300'];
						  
						  $casualImage = get_field('bio_casual_image');
						  $url = $casualImage['url'];
						  $c1200 = $casualImage['sizes']['listing-1200'];
						  $c1026 = $casualImage['sizes']['listing-1026'];
						  $c833 = $casualImage['sizes']['listing-833'];
						  $c605 = $casualImage['sizes']['listing-605'];
						  $c300 = $casualImage['sizes']['listing-300'];
					?>
					<a href="<?php the_permalink();?>">
						<div class="bio-image">
							<figure class="casual-image">
								<img src="<?php echo $c1200; ?>" 
								sizes="(min-width: 960px) 300px,
								(min-width: 768px) calc(32vw - 34px),
								(min-width: 480px) calc(48vw - 34px),
								calc(100vw - 34px)"
								srcset="<?php echo $c300; ?> 300w,
										<?php echo $c605;?> 605w,
										<?php echo $c833;?> 833w,
										<?php echo $c1026;?> 1026w,
										<?php echo $c1200;?> 1200w"			
								alt="" />
							</figure>
							<figure class="professional-image">
								<img src="<?php echo $w1200; ?>" 
								sizes="(min-width: 960px) 300px,
								(min-width: 768px) calc(32vw - 34px),
								(min-width: 480px) calc(48vw - 34px),
								calc(100vw - 34px)"
								srcset="<?php echo $w300; ?> 300w,
										<?php echo $w605;?> 605w,
										<?php echo $w833;?> 833w,
										<?php echo $w1026;?> 1026w,
										<?php echo $w1200;?> 1200w"			
								alt="" />
							</figure>
							<span class="view-bio">View Bio</span>
						</div>
						<div class="bio-info">
							<h4><?php the_title();?></h4>
							<div class="bio-title"><?php the_field('bio_title'); ?></div>
						</div>
					</a>
				</div>
				<?php endwhile; ?>
	
			</div>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
	<?php endwhile; endif;?>
	<?php get_footer(); ?>
</body>
</html>