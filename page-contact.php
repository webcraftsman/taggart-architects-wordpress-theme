<?php
    if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
        wpcf7_enqueue_scripts();
    }
 
    if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
        wpcf7_enqueue_styles();
    }
?>
<?php get_header(); ?>
<body id="page-<?php echo $post->post_name; ?>" class="page-<?php echo $post->post_name; ?> secondary-page">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php include '_includes/banner.php'; ?>
	<div id="content">
		<?php wp_reset_postdata(); ?>
		<div class="wrapper two-column">
			<div class="content-main" role="main">
				<h2 class="page-title"><?php the_title();?></h2>
				<div class="content"><?php echo do_shortcode( '[contact-form-7 id="119" title="Contact Us"]' ); ?>
</div>
				
			</div>
			<aside class="content-secondary" role="complementary">
				<?php if (have_rows('office_locations','option')): ?>
				<?php while (have_rows('office_locations','option')) : the_row(); ?>
				<?php if(get_sub_field('phone_number')):
					$phone = get_sub_field('phone_number');
					$phoneTrimmed = preg_replace("/[^0-9]/", "", $phone);
					endif;
				?>
				<div class="hcard">
					<span class="p-name">Taggart Architects</span><br/>
					<span class="adr"><span class="p-street-address"><?php the_sub_field('street_address');?></span><br/>
					<span class="p-locality"><?php the_sub_field('city');?></span>, <span class="p-region">AR</span> <span class="p-postal-code"><?php the_sub_field('zip_code');?></span></span><br/>
					<?php if(get_sub_field('phone_number')) :?>
					<a href="tel:+1<?php echo $phoneTrimmed; ?>" class="p-tel"><?php echo $phone;?></a>
					<?php endif; ?>
				</div>
				<?php endwhile; ?>
				<?php endif;?>
			</aside>
		</div>
	</div>
		<?php endwhile; endif;?>
	<?php get_footer(); ?>
</body>
</html>