<?php get_header(); ?>
<body id="page-<?php echo $post->post_name; ?>" class="page-<?php echo $post->post_name; ?> bio-page secondary-page">
	<?php include '_includes/banner.php'; ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="content">
		<?php $image = get_field('bio_image');
			  $bio1880 = $image['sizes']['bio-1880'];
			  $bio1536 = $image['sizes']['bio-1536'];
			  $bio940 = $image['sizes']['bio-940'];
			  $bio768 = $image['sizes']['bio-768'];
			  $bio300 = $image['sizes']['bio-300'];
		?>
		<div class="hero-image">
			<img src="<?php echo $bio768; ?>" 
				sizes="(min-width: 940px) 940px,
				calc(100vw - 34px)"
				srcset="<?php echo $bio300; ?> 300w,
						<?php echo $bio768;?> 768w,
						<?php echo $bio940;?> 940w,
						<?php echo $bio1536;?> 1536w,
						<?php echo $bio1880;?> 1880w"			
				alt="" />

		</div>
		<div class="wrapper two-column">
			<div class="content-main" role="main">
				<div class="page-title">
					<h2><?php the_title(); ?></h2>
					<h3 class="page-subtitle"><?php the_field('bio_title'); ?></h3>
				</div>
			<?php if( get_field('bio_inspiration') ): ?>
				<div class="inspiration bio-section">
					<h4 class="bio-subheading">The Inspiration</h4>
					<div class="body-content"><?php the_field('bio_inspiration'); ?></div>
				</div>
			<?php endif; ?>
			<?php if( get_field('bio_education') ): ?>
				<div class="education bio-section">
					<h4 class="bio-subheading">The Education</h4>
					<div class="body-content"><?php the_field('bio_education'); ?></div>			
				</div>
			<?php endif; ?>
			<?php if( get_field('bio_work') ): ?>
				<div class="work bio-section">
					<h4 class="bio-subheading">The Work</h4>
					<div class="body-content"><?php the_field('bio_work'); ?></div>			
				</div>
			<?php endif; ?>
			</div>
			<aside class="content-secondary" role="complementary">
			<?php if( get_field('bio_weekend_image') ): ?>
				<?php $weekendImage = get_field('bio_weekend_image');
					$wk1200 = $weekendImage['sizes']['weekend-1200'];
					$wk1026 = $weekendImage['sizes']['weekend-1026'];
					$wk833 = $weekendImage['sizes']['weekend-833'];
					$wk605 = $weekendImage['sizes']['weekend-605'];
					$wk300 = $weekendImage['sizes']['weekend-300'];
				?>
				<div class="weekend-image">
					<img src="<?php echo $wk605; ?>" 
						sizes="(min-width: 768px) 265px,
						calc(100vw - 34px)"
						srcset="<?php echo $wk300; ?> 300w,
								<?php echo $wk605;?> 606w,
								<?php echo $wk833;?> 833w,
								<?php echo $wk1026;?> 1027w,
								<?php echo $wk1200;?> 1200w"			
						alt="" />
				</div>
			<?php endif; ?>
			<?php if( get_field('bio_weekend') ): ?>
				<h3 class="aside-heading">The Weekend</h3>
				<div class-"body-content"><?php the_field('bio_weekend'); ?></div>
			<?php endif; ?>	
			</aside>
		</div>
		<?php $posts = get_field('bio_projects');
		if( $posts): ?>
		<div id="bio-project-listing">
			<h3 class="section-heading">Projects</h3>
			<ol id="bio-projects-carousel" class="bio-projects-carousel">
				<?php foreach( $posts as $post): ?>
				<?php setup_postdata($post); ?>
				<li class="bio-project">
					<?php $projectImage = get_field('project_featured_image');
						$pc1200 = $projectImage['sizes']['carousel-1200'];
						$pc1027 = $projectImage['sizes']['carousel-1027'];
						$pc833 = $projectImage['sizes']['carousel-833'];
						$pc606 = $projectImage['sizes']['carousel-606'];
						$pc300 = $projectImage['sizes']['carousel-300'];
					?>
					<div class="project-image">
						<a href="<?php the_permalink();?>">
							<img src="<?php echo $pc606; ?>" 
								sizes="(min-width: 768px) 220px,
								(min-size: 600px) calc(48vw - 34px),
								100vw"
								srcset="<?php echo $pc300; ?> 300w,
										<?php echo $pc606;?> 606w,
										<?php echo $pc833;?> 833w,
										<?php echo $pc1027;?> 1027w,
										<?php echo $pc1200;?> 1200w"			
								alt="" />
							<span class="view-bio">View Project</span>
						</a>
					</div>
				</li>
				<?php endforeach; ?>
			</ol>
			<?php wp_reset_postdata();?>
		</div>
		<?php endif; ?>
	</div>
	<?php endwhile; endif;?>
	<?php get_footer(); ?>
</body>
</html>