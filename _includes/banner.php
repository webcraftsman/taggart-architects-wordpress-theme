<header role="banner">
	<h1 class="logo"><a href="/"><?php echo bloginfo('name');?></a></h1>
	<a id="nav-button" href="#footer-nav">Menu</a>
</header>
<nav role="navigation" class="nav-main" aria-hidden="false">
	<ol>
		<li class="nav-profile"><a href="/profile/">Profile</a></li>
		<li class="nav-projects"><a href="/projects/">Projects</a>
			<ol id="project-subnav" class="subnav" aria-hidden="false">
				<li class="nav-healthcare"><a href="/category/healthcare">Healthcare</a></li>
				<li class="nav-education"><a href="/category/education">Education</a></li>
				<li class="nav-government-and-civic"><a href="/category/government-and-civic">Government &amp; Civic</a></li>
				<li class="nav-sports-and-recreation"><a href="/category/sports-and-recreation">Sports &amp; Recreation</a></li>
				<li class="nav-commercial"><a href="/category/commercial">Commercial</a></li>
				<li class="nav-sustainable-design"><a href="/category/sustainable-design">Sustainable Design</a></li>
				<li class="nav-on-the-boards"><a href="/category/on-the-boards">On The Boards</a></li>
			</ol>
		</li>
		<li class="nav-news"><a href="/news/">News</a></li>
		<li class="nav-contact"><a href="/contact/">Contact</a></li>
	</ol>
</nav>