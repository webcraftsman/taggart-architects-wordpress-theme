<!DOCTYPE html>
<html class="no-js<?php if( is_user_logged_in()) { echo ' logged-in'; } ?>" prefix="og: http://ogp.me/ns#">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<?php if (is_front_page()) :?>
			<title><?php echo bloginfo('name');?></title>
		<?php else :?>
			<title><?php wp_title('');?> |  <?php echo bloginfo('name');?></title>
		<?php endif; ?>
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto+Slab:700" rel="stylesheet">
		<?php wp_head(); ?>
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/_css/ie9.css">
		<!--<![endif]-->
		<!--[if lt IE 9]>
			<script src="<?php bloginfo('template_directory'); ?>/_js/html5shiv.js"></script>
		<![endif]-->
	</head>