<?php get_header(); ?>
<body id="page-<?php echo $post->post_name; ?>" class="page-<?php echo $post->post_name; ?> secondary-page single-news-item">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php include '_includes/banner.php'; ?>
	<div id="content">
		<?php wp_reset_postdata(); ?>
		<div class="wrapper two-column">
			<div class="content-main" role="main">
				<?php $featureImage = get_field('news_featured_image');
					if( !empty($featureImage) ):
						$url = $featureImage['url'];
						$news1536 = $featureImage['sizes']['news-1536'];
						$news960 = $featureImage['sizes']['news-960'];
						$news768 = $featureImage['sizes']['news-768'];
						$news480 = $featureImage['sizes']['news-480'];
					endif;
				?>
				<h2 class="page-title"><?php the_title();?></h2>
				<?php if( !empty($featureImage) ): ?>
					<div class="news-image">
						<img src="<?php echo $news768; ?>" 
						sizes="(min-width: 768px) 590px,
						(min-width: 768px) 446px,
						calc(100vw - 34px)"
						srcset="<?php echo $news480; ?> 480w,
								<?php echo $news768;?> 768w,
								<?php echo $news960;?> 960w,
								<?php echo $news1536;?> 1536w"			
						alt="" />
					</div>
				<?php endif; ?>
				<div class="content"><?php the_content();?></div>
			</div>
			<aside class="content-secondary" role="complementary"></aside>
		</div>
	</div>
		<?php endwhile; endif;?>
	<?php get_footer(); ?>
</body>
</html>