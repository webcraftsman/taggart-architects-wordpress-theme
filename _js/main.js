jQuery(function ($) {
	
	if ($('#nav-button').length) {
		$('#nav-button').on('click',function(e) {
			e.preventDefault();
			$('[role=banner]').toggleClass('show');
		});
	}
	
	// Project Listing Filter
	if ( $('body').hasClass('page-project-listing') )
	{
		if ($('#project-subnav').length) 
		{
			$('#project-subnav a').click(function(e) {
				e.preventDefault();
				$('#project-subnav a').each(function() {
					$(this).parent().removeClass('active');
				});
				$(this).parent().addClass('active');
				var href = $(this).attr('href');
				var hash = href.substring(href.lastIndexOf("/") + 1);
				console.log(hash);
				$('.project').fadeOut(250).delay(175);
				$('.'+hash).each(function(fadeInDiv){
					$(this).delay(fadeInDiv * 100).fadeIn(250);
				});
			});
		}
	}
	// Redirect project category to projects page
	else {
		$('#project-subnav a').click(function(e) {
			e.preventDefault();
			var href = $(this).attr('href');
			var destination = href.substring(href.lastIndexOf("/") + 1);
			console.log(destination);
			window.location.href = '/projects?category=' + destination;
			
		});
	}
	
	// If project page is redirect from subnav, this script add the category filter
	
	if (window.location.href.indexOf("?category") > -1) {
	    function getQueryVariable(variable)
	    {
		    var query = window.location.search.substring(1);
		    var vars = query.split('&');
		    for (var i=0;i<vars.length;i++)
		    {
			    var pair = vars[i].split('=');
			    if(pair[0] == variable) { return pair[1];}
		    }
		    return(false);
	    }
	    
	    var $category = getQueryVariable('category');
	    var nav = '.nav-'+$category;
	    $(nav).addClass('active');
	    $('.project').not('.'+$category).hide();
	}
	
	
	// Hero Carousel on Single Project Page
	if ( $('body').hasClass('single-project-page') )
	{
		$('#hero-carousel ol').slick({
			slide:'li',
			infinite: true,
	    	slidesToShow:1,
	    	slidesToScroll:1,
	    	autoplay:true,
	    	dots:true,
	    	arrows:true,
	    	speed:500,
	    	cssEase: 'linear',
	    	autoplaySpeed:5000,
	    	mobileFirst: true,
	    	responsive: [{
		    	breakpoint: 660,
		    	settings: {
			    	centerMode:true,
			    	centerPadding:0,
			    	autoplay:false,
			    	fade:false,
			    	arrows:true,
		    	}
	    	}]
		});
		
		$('#projects-carousel').slick({
			slide:'li',
			infinite: true,
	    	slidesToShow:1,
	    	slidesToScroll:1,
	    	autoplay:false,
	    	dots:true,
	    	arrows:true,
	    	speed:500,
	    	cssEase: 'linear',
	    	mobileFirst: true,
	    	responsive: [{
		    	breakpoint: 600,
		    	settings: {
			    	slidesToShow:2,
			    	slidesToScroll:1,
			    	autoplay:false,
			    	fade:false,
			    	dots:false
		    	}
	    	}, {
	    		breakpoint: 768,
		    	settings: {
			    	slidesToShow:3,
					slidesToScroll:1,
			    	autoplay:false,
			    	fade:false,
			    	dots:false
		    	}
	    	},
	    		{
	    		breakpoint: 960,
		    	settings: {
			    	slidesToShow:4,
			    	slidesToScroll:1,
			    	autoplay:false,
			    	fade:false,
			    	dots:false
		    	}
	    	}]
		});
	}
	if ( $('body').hasClass('bio-page') )
	{
		$('#bio-projects-carousel').slick({
			slide:'li',
			infinite: true,
	    	slidesToShow:1,
	    	slidesToScroll:1,
	    	autoplay:false,
	    	dots:true,
	    	arrows:true,
	    	speed:500,
	    	cssEase: 'linear',
	    	mobileFirst: true,
	    	responsive: [{
		    	breakpoint: 600,
		    	settings: {
			    	slidesToShow:2,
			    	slidesToScroll:1,
			    	autoplay:false,
			    	fade:false,
			    	dots:false
		    	}
	    	}, {
	    		breakpoint: 768,
		    	settings: {
			    	slidesToShow:3,
					slidesToScroll:1,
			    	autoplay:false,
			    	fade:false,
			    	dots:false
		    	}
	    	}, {
	    		breakpoint: 960,
		    	settings: {
			    	slidesToShow:4,
			    	slidesToScroll:1,
			    	autoplay:false,
			    	fade:false,
			    	dots:false
		    	}
	    	}]
		});
	}
})





function TaggartResize ( $, window ) {
	window.watchResize = function( callback )
	{
		var resizing;
		function done()
		{
			clearTimeout( resizing );
			resizing = null;
			callback();
		}
		$(window).resize(function(){
			if ( resizing )
			{
				clearTimeout( resizing );
				resizing = null;
			}
			resizing = setTimeout( done, 50 );
		});
		// init
		callback();
	};
	window.watchResize(function(){
		var size = $(window).width();
		
		if ( $('body').hasClass('single-project-page') )
		{
			if ( size > 768) 
			{
				$('.project-dev a').on('click', function(e){
					e.preventDefault();
					var modalBase = $(this).data('modal');
					var modalCaption = $(this).data('caption');
					var modalImage = '<img src="' + modalBase + '-1222x820.jpg" sizes="100vw" srcset="' + modalBase + '-1222x820.jpg 1222w, ' + modalBase + '-1518x1019.jpg 1518w, ' + modalBase + '-1817x1219.jpg 1817w, ' + modalBase + '-2074x1392.jpg 2074w, ' + modalBase + '-2322x1558.jpg 2322w, ' + modalBase + '-2560x1718.jpg 2560w, ' + modalBase + '-2875x1869.jpg 2875w, ' + modalBase + '.jpg 3000w" alt="" />';
					
									
					var modal = '<div id="modal"><span class="close-btn"></span><div class="wrapper"><figure>' + modalImage + '<figcaption>' + modalCaption + '</figure></wrapper></div>';
					$('body').append(modal).addClass('modal-open');
					
				});
				
				$('.hero-project a, .hero-slide a').on('click', function(e){
					e.preventDefault();
					var modalBase = $(this).data('modal');
					var modalImage = '<img src="' + modalBase + '-1222x820.jpg" sizes="100vw" srcset="' + modalBase + '-1222x820.jpg 1222w, ' + modalBase + '-1518x1019.jpg 1518w, ' + modalBase + '-1817x1219.jpg 1817w, ' + modalBase + '-2074x1392.jpg 2074w, ' + modalBase + '-2322x1558.jpg 2322w, ' + modalBase + '-2560x1718.jpg 2560w, ' + modalBase + '-2875x1869.jpg 2875w, ' + modalBase + '.jpg 3000w" alt="" />';
					
									
					var modal = '<div id="modal"><span class="close-btn"></span><div class="wrapper"><figure>' + modalImage + '</figure></wrapper></div>';
					$('body').append(modal).addClass('modal-open');
					
				});
				
				
				$(document).on('click','#modal .close-btn', function(){
					$('body').removeClass('modal-open');
					$('#modal').remove();
				});
			}
		}
	});
}
TaggartResize( jQuery, window );
