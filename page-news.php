<?php get_header(); ?>
<body id="page-<?php echo $post->post_name; ?>" class="page-<?php echo $post->post_name; ?> secondary-page news-listing">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php include '_includes/banner.php'; ?>
	<div id="content">
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="hero-image"><?php the_post_thumbnail(); ?></div>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
		<div class="wrapper two-column">
			<div class="content-main" role="main">
				<h2 class="page-title">News</h2>
				<?php
					$args= array(
						'post_per_page' => '5',
						'post_type' => 'news_item',
						'order' => 'DESC'
					);
					
					$args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
					$the_query = new WP_Query( $args );
				
					$temp_query = $wp_query;
					$wp_query = NULL;
					$wp_query = $the_query;
				?>
				<?php if( $wp_query->have_posts() ): ?>
				<div class="news-listing">
					<?php while( $wp_query->have_posts() ) : $wp_query->the_post();?>
					<?php $featureImage = get_field('news_featured_image');
						if( !empty($featureImage) ):
							$url = $featureImage['url'];
							$news1536 = $featureImage['sizes']['news-1536'];
							$news960 = $featureImage['sizes']['news-960'];
							$news768 = $featureImage['sizes']['news-768'];
							$news480 = $featureImage['sizes']['news-480'];
						endif;
					?>
					<article class="news-item">
						<div class="posting-date"><span><strong><?php the_time('F j,'); ?></strong> <?php the_time('Y'); ?></span></div>
						<?php if( !empty($featureImage) ): ?>
						<div class="news-image">
							<img src="<?php echo $news768; ?>" 
							sizes="(min-width: 768px) 590px,
							(min-width: 768px) 446px,
							calc(100vw - 34px)"
							srcset="<?php echo $news480; ?> 480w,
									<?php echo $news768;?> 768w,
									<?php echo $news960;?> 960w,
									<?php echo $news1536;?> 1536w"			
							alt="" />
						</div>
						<?php endif; ?>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<div class="excerpt"><?php the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>" class="read-full-article">read full article</a></div>
					</article>
					<?php endwhile; endif;?>
				</div>
				<?php $total_pages = $wp_query->found_posts;
				if ($total_pages > 5) : ?>
				<div class="pagination">
					<?php echo paginate_links(array(
						'prev_next' => false,
						'end_size' => 1,
						'mid_size' => 5
						)); ?>
				</div>
				<?php endif; ?>
				<?php 
				  $wp_query = null; 
				  $wp_query = $temp;  // Reset
				?>
				<?php wp_reset_query(); ?>
			</div>
			<aside class="content-secondary" role="complementary">
<!--				<?php if( get_field('news_sidebar') ): ?>
					<?php the_field('news_sidebar'); ?>
				<?php endif; ?> -->
				<div class="facebook-news-feed">
				<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTAGGART-Architects-171155099600266%2F&tabs=timeline&width=265px&height=1000px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="265px" height="1000px" style="border:none;overflow:none" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				</div>
			</aside>
		</div>
	</div>
	<?php endwhile; endif;?>
	<?php get_footer(); ?>
</body>
</html>