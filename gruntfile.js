module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				options: {
					style: 'expanded'
				},
				files: {
					'_css/main.css' : '_source/_scss/main.scss'
				}
			}
		},
		postcss: {
			options: {
				processors : [
					require('autoprefixer')({browsers:['last 2 versions', 'ie 9-11']}),
					require('cssnano')({
						minifyFont: false
					})
				]
			},
			dist: {
				src:'_css/main.css',
				dest: '_css/main.min.css'
			}
		},
		concat: {   
			dist: {
				src: ['_source/_js/main.js'],
				dest: '_js/main.js'
			}
		},
		uglify: {
			build: {
				src: '_js/main.js',
				dest: '_js/main.min.js'
			}
		},
		legacssy: {
			dist: {
				options: {
					legacyWidth: 980
				},
				files: {
					'_css/main-legacy.css': '_css/main.css'
				}
			}
		},
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass','legacssy','postcss']
			},
			scripts: {
				files:'_source/_js/*.js',
				tasks:['concat','uglify']
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-legacssy');
	grunt.loadNpmTasks('grunt-postcss');

	grunt.registerTask('default',['concat','uglify']);
    grunt.registerTask('imageoptim', ['imageoptim']);
}