<?php get_header(); ?>
<body id="page-<?php echo $post->post_name; ?>" class="page-<?php echo $post->post_name; ?>">
	<?php include '_includes/banner.php'; ?>
	<div id="content" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="hero-image"><?php the_post_thumbnail(); ?></div>
		<?php endif; ?>
		<div class="home-intro-content">
			<?php the_content();?>
			<a href="/profile/" class="btn">Firm Profile</a>
		</div>
	<?php endwhile; endif;?>
	</div>
	<?php get_footer(); ?>
</body>
</html>